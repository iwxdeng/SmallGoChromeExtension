$(function () {
  $('#save').on('click',function () {
    var adzoneid          = $('#adzoneid').val();
    var siteid            = $('#siteid').val();
    var domain           = $('#domain').val();

    if (!adzoneid) {
      alert('请设置adzoneid');
      return;
    }
    if (!siteid) {
      alert('请设置siteid');
      return;
    }
    if (!domain) {
      alert('请设置淘客网站域名');
      return;
    }
    chrome.storage.sync.set({'adzoneid': adzoneid}, function() {
      console.log('Settings saved');
    });
    chrome.storage.sync.set({'siteid': siteid}, function() {
      console.log('Settings saved');
    });
    chrome.storage.sync.set({'domain': domain}, function() {
      console.log('Settings saved');
    });
    alert('保存成功')
  })
})

chrome.storage.sync.get('adzoneid', function(item) {
  if(item){
    $('#adzoneid').val(item.adzoneid);
  }
});
chrome.storage.sync.get('siteid', function(item) {
  if(item){
    $('#siteid').val(item.siteid);
  }
});
chrome.storage.sync.get('domain', function(item) {
  if(item){
    $('#domain').val(item.domain);
  }
});