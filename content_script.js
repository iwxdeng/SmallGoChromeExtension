﻿function getCookie(name) {
  var strCookie = document.cookie;
  var arrCookie = strCookie.split("; ");
  for (var i = 0; i < arrCookie.length; i++) {
    var arr = arrCookie[i].split("=");
    if (arr[0] == name) return arr[1];
  }
  return "";
}

var adzoneid = '';
var siteid = '';
var domain = ''
var lastChannelId ='0';
var lastPageNo  = '0';
var lastIndex = '0';
var lastTotalPages= '0';
var lastPerPageSize ='0';
var lastVolume = '';
var lastStartPrice ='';
var lastEndPrice ='';
chrome.storage.sync.get('adzoneid', function (item) {
  adzoneid = item.adzoneid;
});
chrome.storage.sync.get('siteid', function (item) {
  siteid = item.siteid;
});
chrome.storage.sync.get('domain', function (item) {
  domain = item.domain;
});
chrome.storage.sync.get('lastChannelId', function (item) {
  lastChannelId = item.lastChannelId;
});
chrome.storage.sync.get('lastPageNo', function (item) {
  lastPageNo = item.lastPageNo;
});
chrome.storage.sync.get('lastIndex', function (item) {
  lastIndex = item.lastIndex;
});
chrome.storage.sync.get('lastTotalPages', function (item) {
  lastTotalPages = item.lastTotalPages;
});
chrome.storage.sync.get('lastPerPageSize', function (item) {
  lastPerPageSize = item.lastPerPageSize;
});
chrome.storage.sync.get('lastEndPrice', function (item) {
  lastEndPrice = item.lastEndPrice;
});
chrome.storage.sync.get('lastStartPrice', function (item) {
  lastStartPrice = item.lastStartPrice;
});
chrome.storage.sync.get('lastVolume', function (item) {
  lastVolume = item.lastVolume;
});




//添加组件
var bar = $('<div></div>');
var form = $('<form></form>');
var title = $('<p>上次采集信息</p>');
var lastTips=$('<p></p>')
var startPage = $('<input placeholder="起始页">');
var showToolsButton = $('<div id="showTools" title="显示阿里妈妈辅助工具栏">显示</div>')
var channelGroup = $('<div class="chrome-group"><label>频道Id</label><div class="chrome-control"><input name="channel" type="text" placeholder="地址栏中\'channel=\'后面的参数"></div></div>')
var userTypeGroup = $('<div class="chrome-group"><label>淘宝商品</label><div class="chrome-control"><input name="userType" type="checkbox"></div></div>');
var b2cGroup = $('<div class="chrome-group"><label>天猫商品</label><div class="chrome-control"><input name="b2c" type="checkbox"></div></div>');
var startPageGroup = $('<div class="chrome-group"><label>起始页</label><div class="chrome-control"><input name="toPage" type="number"></div></div>');
var jpmjeGroup = $('<div class="chrome-group"><label>金牌卖家</label><div class="chrome-control"><input name="jpmj" type="checkbox"></div></div>');
var dpyhqGroup = $('<div class="chrome-group"><label>店铺优惠券</label><div class="chrome-control"><input name="dpyhq" type="checkbox"></div></div>');
var startBiz30dayGroup = $('<div class="chrome-group"><label>月销量</label><div class="chrome-control"><input name="startBiz30day" type="number"></div></div>');
var startPriceGroup = $('<div class="chrome-group"><label>起始价格</label><div class="chrome-control"><input name="startPrice" type="number" value="0"></div></div>');
var endPriceGroup = $('<div class="chrome-group"><label>结束价格</label><div class="chrome-control"><input name="endPriceInput" type="number"></div></div>');
var channelIdGroup = $('<div class="chrome-group"><label>采集到频道</label><div class="chrome-control"><select name="channel_id"><option value="0">选择频道</option></select></div></div>');
var categoryGroup = $('<div class="chrome-group"><label>采集到分类</label><div class="chrome-control"><select name="category_id"><option value="0">选择分类</option></select></div></div>');
var hideGroup = $('<div class="chrome-group"> <button class="chrome-btn chrome-btn-lg chrome-margin-auto chrome-btn-info" id="hide" type="button">隐藏工具栏</button></div>');
var buttonGroup = $('<div class="chrome-group"> <button class="chrome-btn chrome-btn-lg chrome-margin-auto"  type="button">开始采集</button></div>');
var stopGroup = $('<div class="chrome-group"> <button class="chrome-btn chrome-btn-lg chrome-margin-auto chrome-btn-error" type="button">停止采集</button></div>');
var tips = $('<p class="chrome-title">开始采集...</p>');
var goodsTips = $('<p class="chrome-title"></p>');
var userTypeInput = userTypeGroup.find('input').eq(0);
var jpmjInput = jpmjeGroup.find('input').eq(0);
var dpyhqInput = dpyhqGroup.find('input').eq(0);
var startPriceInput = startPriceGroup.find('input').eq(0);
var endPriceInput = endPriceGroup.find('input').eq(0);
var channelInput = channelGroup.find('input').eq(0);
var categorySelect = categoryGroup.find('select').eq(0);
var startPageInput = startPageGroup.find('input').eq(0);
var channelSelect = channelIdGroup.find('select').eq(0);
var button = buttonGroup.find('button').eq(0);
var hideButton = hideGroup.find('button').eq(0);
var stop = stopGroup.find('button').eq(0);
//var perPageSize           = $('<input name="perPageSzie" type="number">');
var pageTimer = null;
var itemTimer = null;
var toPage = 1;
var pages = toPage + 1;
var current = 1;//分页当前循环索引
var total = 1;
var wait=0;
var uploadWaitTime        = 0;
var i=0;//分页循环索引

function init() {
  form.append(channelGroup);
  form.append(userTypeGroup);
  form.append(b2cGroup);
  form.append(startPageGroup);
  form.append(jpmjeGroup);
  form.append(dpyhqGroup);
  form.append(startBiz30dayGroup);
  form.append(startPriceGroup);
  form.append(endPriceGroup);
  form.append(channelIdGroup);
  form.append(categoryGroup);
  form.append(hideGroup);
  form.append(buttonGroup);
  form.append(stopGroup);
  stopGroup.hide();
  button.addClass('chrome-btn').addClass('btn-size28');
  title.addClass('chrome-title');
  channelInput.addClass('chrome-channel-input');
  startPage.addClass('chrome-channel-input');
  channelSelect.addClass('chrome-select');
  categorySelect.addClass('chrome-select');
  bar.addClass('chrome-bar');
  bar.append(title);
  bar.append(lastTips);
  bar.append(form)

  $('body').append(bar);
  $('body').append(showToolsButton);
  channelOptions(channelSelect);
  categoryOptions(categorySelect);
  button.on('click', function () {
    var channelId = channelInput.val();
    toPage = startPageInput.val() ? startPageInput.val() : 1;
    pages = toPage + 1;
    if (channelId) {
      collect(channelId);
      bar.append(tips);
      bar.append(goodsTips);
      button.text('开始采集');
      buttonGroup.hide();
      stopGroup.show();
    } else {
      alert('请填写频道Id');
    }
  });
  stop.on('click', function () {
    clearInterval(pageTimer);
    clearInterval(itemTimer);
    isCollecting = false;
    tips.text('已停止采集');
    stopGroup.hide();
    buttonGroup.show();
    button.text('继续采集');
  });
  hideButton.on('click',function () {
    bar.fadeOut();
    console.log('隐藏')
    console.log(lastPageNo);
  });
  showToolsButton.on('click',function () {
    bar.fadeIn();
    console.log('显示')
  });
}

var channlUrl = 'https://pub.alimama.com/items/channel/';
var searchUrl = 'https://pub.alimama.com/items/search.json';
var perPageSize = 50;

var isCollecting = false;

function collect(channelId) {
  chrome.storage.sync.set({'lastChannelId': channelId}, function() {

  });
  chrome.storage.sync.set({'lastPerPageSize': perPageSize}, function() {

  });
  pageTimer = setInterval(function () {
    if (pages > toPage) {
      if (!isCollecting) {
        tips.text('正在采集第' + toPage + '页');
        chrome.storage.sync.set({'lastPageNo': toPage}, function() {
          console.log('Settings saved '+toPage);
        });
        getChannelPageData(channelId);
      }
    } else {
      clearInterval(pageTimer);
    }
  }, 5000)
}

function isSetting() {
  if (adzoneid==='') {
    alert('请前往插件设置adzoneid');
    return false;
  }
  if (siteid==='') {
    alert('请前往插件设置siteid');
    return false;
  }
  if (domain==='') {
    alert('请前往插件设置淘客网站域名');
    return false;
  }
  return true;
}

function categoryOptions(select) {
  $.get(domain + '/category/option', {}, function (data) {
    if (data) {
      for (var i = 0; i < data.length; i++) {
        var option = $('<option>' + data[i].name + '</option>');
        option.val(data[i].id)
        select.append(option);
      }
      return select;
    }
  })
}

function channelOptions(select) {
  $.get(domain + '/channel/option', {}, function (data) {
    if (data) {
      for (var i = 0; i < data.length; i++) {
        var option = $('<option>' + data[i].name + '</option>');
        option.val(data[i].id)
        select.append(option);
      }
      return select;
    }
  })
}
function upload(item,action) {
  var query1 = {
    'num_iid': item.auctionId,
    'title': item.title,
    'pict_url': item.pictUrl,
    'item_url': item.auctionUrl,
    'zk_final_price': item.zkPrice,
    'status': 1,
    'volume': item.biz30day,
    'coupon_info': item.couponInfo,
    'click_url': action.clickUrl,
    'coupon_click_url': action.couponLink,
    'tpwd': action.taoToken,
    'category_id': categorySelect.val(),
    'channel_id': channelSelect.val(),
    'coupon_start_time': item.couponEffectiveStartTime,
    'coupon_end_time': item.couponEffectiveEndTime,
    'coupon_remain_count': item.couponLeftCount
  };
  $.post(domain + '/taobao/client/collect', query1, function (response) {
    i++;
    total++;
  });
}
function getActionCode(item) {
  if(wait==0){
    var query = {
      auctionid: item.auctionId,
      adzoneid: adzoneid,
      siteid: siteid,
      scenes: '1',
      _tb_token_: getCookie('_tb_token_')
    }
    //从联盟转换淘口令
    $.get('https://pub.alimama.com/common/code/getAuctionCode.json', query, function (response) {
      if (response.status) {
        if (response.wait) {
          alert('接口被联盟限制，请等待' + response.wait + '秒后点击确认重试!');
          wait(response.wait);
        }
        return;
      }
      goodsTips.text(total + '：' + item.title);
      if (response.info.ok) {
        //成功获取后上传
        upload(item,response.data);
      } else if (response.info.message == 'nologin') {
        alert('请先登录淘宝联盟');
        clearInterval(pageTimer);
        clearInterval(itemTimer);
        return;
      } else {
        alert(item.auctionId + response.info.message)
      }
    });
  }

}

function getChannelPageData(channelId) {
  //处理参数
  var params = formToJson(form);
  params['shopTag'] = '';
  if (params.jpmj) {
    params.shopTag = 'jpmj';
  }
  if (params.dpyhq) {
    if (params.shopTag == '') {
      params.shopTag = 'dpyhq';
    } else {
      params.shopTag += ',dpyhq';
    }
  }
  if (params.b2c) {
    if (params.shopTag == '') {
      params.shopTag = 'b2c';
    } else {
      params.shopTag += ',b2c';
    }
  }
  params.category_id = null;
  params.channel_id = null;
  params.toPage = toPage;
  params['perPageSize'] = perPageSize;
  if(wait ==0 ){
    $.get(channlUrl + channelId + '.json', params, function (data) {
      if (data.data) {
        if (data.status) {
          if (data.wait) {
            alert('接口被联盟限制，请等待' + data.wait + '秒后点击确认重试!');
            wait(data.wait)
          }
          return;
        }else {
          toPage++;
          //循环商品
          if (data.data.pageList) {
            pages = data.data.paginator.pages;
            chrome.storage.sync.set({'lastTotalPages': pages}, function() {

            });
            itemTimer = setInterval(function () {
              chrome.storage.sync.set({'lastIndex': i}, function() {

              });
              if (i < data.data.pageList.length) {
                isCollecting = true;
                if (current == i) {
                  uploadWaitTime++;
                  //上传商品超时，执行重试
                  if(uploadWaitTime<10){
                    return;
                  }
                }
                current = i;
                var item = data.data.pageList[i];
                getActionCode(item);
              }else {
                i=0;
                  clearInterval(itemTimer);
                  isCollecting = false;
                  if (toPage >= pages) {
                    tips.text('采集完成,共采集' + total + '件商品');
                    buttonGroup.show();
                    button.text('开始采集');
                    stopGroup.hide();
                    clearInterval(pageTimer)
                  }
                }
            },3000);
          }
        }

      }
    });
  }

}

function wait(time) {
  setTimeout(function () {
    if(wait > time){
      wait      = 0;
    }else {
      wait++;
      wait(time);
    }
  },1000)
}

function getChanellGoods() {

}
function formToJson(form) {
  var o = {};
  var a = form.serializeArray();
  $.each(a, function () {
    var name = this.name;
    var value = this.value;
    var paths = this.name.split(".");
    var len = paths.length;
    var obj = o;
    $.each(paths, function (i, e) {
      if (i == len - 1) {
        if (obj[e]) {
          if (!obj[e].push) {
            obj[e] = [obj[e]];
          }
          obj[e].push(value || '');
        } else {
          obj[e] = value || '';
        }
      } else {
        if (!obj[e]) {
          obj[e] = {};
        }
      }
      obj = o[e];
    });
  });
  return o;
};

$(document).ready(function () {
  lastTips.text('频道:'+lastChannelId+';分页进度:'+lastPageNo+'/'+lastTotalPages+';单页进度:'+lastIndex+'/'+lastPerPageSize);
  channelInput.val(lastChannelId);
  var start                   = lastPerPageSize>lastIndex ? lastPageNo : parseInt(lastPageNo)+1;
  startPageInput.val(start);
  startPriceInput.val(lastStartPrice);
  endPriceInput.val(lastEndPrice);
  if(window.location.pathname.indexOf('myunion')<0){
    setTimeout(function () {
      if(isSetting()){
        init();
      }
    }, 3000)
  }
});